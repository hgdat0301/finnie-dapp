import {
  Transaction,
  SystemProgram,
  PublicKey,
  LAMPORTS_PER_SOL,
  Connection,
  clusterApiUrl
} from '@solana/web3.js'

export const createNewTransaction = async () => {
  const connection = new Connection(clusterApiUrl('devnet'), 'confirmed')

  let transaction = new Transaction().add(
    SystemProgram.transfer({
      fromPubkey: new PublicKey('BMwPgjEUXn3igiP7Qebta4cM68WPN9yoMyQLua7RMFCC'),
      toPubkey: new PublicKey('BMwPgjEUXn3igiP7Qebta4cM68WPN9yoMyQLua7RMFCC'),
      lamports: LAMPORTS_PER_SOL * 0.01
    })
  )

  transaction.feePayer = new PublicKey(
    'BMwPgjEUXn3igiP7Qebta4cM68WPN9yoMyQLua7RMFCC'
  )

  const latestBlockhash = await connection.getLatestBlockhash()

  transaction.recentBlockhash = latestBlockhash.blockhash
  transaction.lastValidBlockHeight = latestBlockhash.lastValidBlockHeight

  return transaction
}
