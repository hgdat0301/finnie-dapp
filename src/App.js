import './App.css'
import { useEffect, useState } from 'react'
import { createNewTransaction } from './utils'


function App() {
  const [disabledButton, setDisabledButton] = useState(true)

  const signTx = async () => {
    await window.solana.connect()
    const transaction = await createNewTransaction()
    await window.solana.signTransaction(transaction)

    console.log(transaction)
  }

  const signAllTx = async () => {
    await window.solana.connect()
    const transaction = await createNewTransaction()
    await window.solana.signAllTransactions([transaction])

    console.log(transaction)
  }

  const signAndSendTx = async () => {
    await window.solana.connect()
    const transaction = await createNewTransaction()
    const result = await window.solana.signAndSendTransaction(transaction.serializeMessage())

    console.log(result)
  }

  useEffect(() => {
    window.addEventListener('DOMContentLoaded', () => {
      setDisabledButton(false)
    })
  }, [])

  return (
    <div className="App">
      <button disabled={disabledButton} onClick={signTx}>
        sign transaction
      </button>
      <button disabled={disabledButton} onClick={signAllTx}>
        sign all transaction
      </button>
      <button disabled={disabledButton} onClick={signAndSendTx}>
        sign and send transaction
      </button>
    </div>
  )
}

export default App
